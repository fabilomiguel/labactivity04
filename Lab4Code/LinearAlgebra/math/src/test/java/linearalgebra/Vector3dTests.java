package linearalgebra;
import static org.junit.Assert.*;
import org.junit.Test;


public class Vector3dTests 
{

    @Test
    public void shouldReturnValueOfX()
    {
        Vector3d v1 = new Vector3d(1.5, 0, 0);
        assertEquals(1.5, v1.getX(), 0.0001);
    }

    @Test
    public void shouldReturnValueOfY()
    {
        Vector3d v1 = new Vector3d(0, 16.9978, 0);
        assertEquals(16.9978, v1.getY(), 0.0001);
    }
    
    @Test
    public void shouldReturnValueOfZ()
    {
        Vector3d v1 = new Vector3d(0, 0, 798.87);
        assertEquals(798.87, v1.getZ(), 0.0001);
    }

    @Test
    public void ReturnGoodValueMagnitude()
    {
        Vector3d v1 = new Vector3d(1, 2, 3);

        assertEquals(3.7416, v1.magnitude(), 0.0001);
    }

     @Test
    public void ReturnGoodValueDotProduct()
    {
        Vector3d v1 = new Vector3d(1, 2, 3);
        Vector3d v2 = new Vector3d(4, 4, 4);

        assertEquals(24, v1.dotProduct(v2), 0.0001);
    }

    @Test
    public void ReturnXinAddVector()
    {
        Vector3d v1 = new Vector3d(1, 2, 3);
        Vector3d v2 = new Vector3d(4, 4, 4);   

         assertEquals(5, v1.add(v2).getX(), 0.0001);
    }

    
    @Test
    public void ReturnYinAddVector()
    {
        Vector3d v1 = new Vector3d(1, 2, 3);
        Vector3d v2 = new Vector3d(4, 4, 4);   

         assertEquals(6, v1.add(v2).getY(), 0.0001);
    }

    
    @Test
    public void ReturnZinAddVector()
    {
        Vector3d v1 = new Vector3d(1, 2, 3);
        Vector3d v2 = new Vector3d(4, 4, 4);   

         assertEquals(7, v1.add(v2).getZ(), 0.0001);
    }
}
