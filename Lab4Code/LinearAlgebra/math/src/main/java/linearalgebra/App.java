package linearalgebra;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main(String[] args) 
    {
        //creating vectors
        Vector3d v1 = new Vector3d(1, 2, 3);
        Vector3d v2 = new Vector3d(4, 4, 4);
        Vector3d v3 = new Vector3d(0, 0, 0);
        
        //testing magnitude
        double magnitudeOfv1 = v1.magnitude();
        double magnitudeOfv2 = v2.magnitude();
        double magnitudeOfv3 = v3.magnitude();

        System.out.println(magnitudeOfv1+", "+ magnitudeOfv2+", "+ magnitudeOfv3+".");

        //testing dotproduct
        double dotProductOfv1v2 = v1.dotProduct(v2);

        System.out.println(dotProductOfv1v2);

        //testing add
        Vector3d addVector3dv1v3 = new Vector3d(0,0,0);
        addVector3dv1v3 = v1.add(v3);
        Vector3d addVector3dv1v2 = new Vector3d(0,0,0);
        addVector3dv1v2 = v1.add(v2);

         System.out.println("X value: "+ addVector3dv1v3.getX()+", Y value: "+ addVector3dv1v3.getY()+", Z value: "+ addVector3dv1v3.getZ()+".");
         
         System.out.println("X value: "+ addVector3dv1v2.getX()+", Y value: "+ addVector3dv1v2.getY()+", Z value: "+ addVector3dv1v2.getZ()+".");


    }
}
