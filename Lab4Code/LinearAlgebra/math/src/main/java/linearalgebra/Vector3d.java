package linearalgebra;
//Fabilo Miguel de Lima Silva
public class Vector3d 
{
    private double x;
    private double y;
    private double z;

//constructor 
    public Vector3d(double x, double y, double z) 
    {
        this.x = x;
        this.y = y;
        this.z = z;

    }
//getters
    public double getX()
    {
        return this.x;
    }
    public double getY()
    {
        return this.y;
    }
    public double getZ()
    {
        return this.z;
    }
//methods
    public double magnitude()
    {
        double magnitude = Math.sqrt(x*x+y*y+z*z);
        return magnitude;
    }

    public double dotProduct(Vector3d vector)
    {
        double dotProduct = (vector.getX()*this.x + vector.getY()*this.y + vector.getZ()*this.z);
        return dotProduct;
    }

    public Vector3d add(Vector3d vector)
    {
        double newX = vector.getX()+this.x;
        double newY = vector.getY()+this.y;
        double newZ = vector.getZ()+this.z;
        Vector3d addedVector = new Vector3d(newX, newY, newZ);
        return addedVector;
    }

    
}
